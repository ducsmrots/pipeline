// library("default@master")
import org.jenkinsci.plugins.pipeline.modeldefinition.Utils

// def call(image, branch_name) {
    parallel(
       'SIT': {
        stages {
         stage('Build docker image') {
         when {
            branch 'master'
         }
            steps {
               script {
                  sh "sudo docker build . -t ${image}:${branch_name}"
               }
            }
         }
         stage('Integration test') {
         when {
               branch 'master'
         }
               steps {
                  script {
                     sh "sudo container_name=${image} image_tag=${image}:${branch_name} docker-compose up -d"
                     sh "sudo docker rmi -f ${image}:${branch_name}"
                     // sh "sudo docker container rm ${image}"
                  }
               }
         }
         stage('Helm deploy') {
         when {
               branch 'master'
         }
               steps {
               script {
                  sh "git clone https://gitlab.com/ducsmrots/helm.git"
                  sh "helm dependency update helm/kube_chart"
                  sh "helm install ${image} helm/kube_chart --debug --dry-run"
               }
               }    
         }
       }  
      }
    )
 // }
