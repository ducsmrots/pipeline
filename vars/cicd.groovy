import org.jenkinsci.plugins.pipeline.modeldefinition.Utils

def call() {

  def config = readYaml text: "${env.PIPELINE_CONFIG}"
  def jobnameparts = JOB_NAME.tokenize('/') as String[]
  def image = jobnameparts[0]


pipeline {
        agent {
        label 'worker1'
        }

        environment {
            ms_name = "${config.microservice_name}"
            ms_version = "${config.microservice_version}"
            dtr_creds = credentials('dtr_creds')
            dtr_repo = "docker.io/ducsmrots"
        }
        
        stages {         
            stage("master") {
                stages {            
                stage('Build docker image') {
                when {
                    branch 'SIT'
                }
                    steps {
                        script {
                            sh "sudo docker login -u ${dtr_creds_USR} -p ${dtr_creds_PSW}"
                            sh "sudo docker build . -t ${dtr_repo}/${ms_name}:${ms_version}"
                        }
                    }
                }
                stage('Integration test') {
                when {
                    branch 'SIT'
                }
                    steps {
                        script {
                            sh "sudo container_name=${ms_name} image_tag=${ms_name}:${ms_version} docker-compose up -d"
                            sh "sudo docker rmi -f ${ms_name}:${ms_version}"
                            // sh "sudo docker container rm ${image}"
                        }
                    }
                }

                stage('Push Image to DTR SIT') {
                when {
                    branch 'SIT'
                }
                   steps {
                       script{
                           sh "sudo docker push ${dtr_repo}/${ms_name}:${ms_version}"
                       }
                   }
                }

                stage('Helm deploy SIT') {
                when {
                    branch 'SIT'
                }
                    steps {
                        script {
                            sh "git clone https://gitlab.com/ducsmrots/helm.git"
                            sh "helm dependency update helm/kube_chart"
                            sh "helm install ${image} helm/kube_chart --debug --dry-run"
                        }
                    }    
                }
              } // stages
            } // SIT

              stage("UAT") {
                stages {            
                stage('Push Image UAT') {
                when {
                    branch 'SIT'
                }
                    steps {
                        script {
                            println "pulling image UAT ${ms_name}:${ms_version}"
                        }
                    }
                }
                stage('Prepare manifest files') {
                when {
                    branch 'SIT'
                }
                    steps {
                        script {
                            println "Prepare manifest files SIT"
                            
                        }
                    }
                }
                stage('Helm deploy') {
                when {
                    branch 'SIT'
               }
                     steps {
                        script {
                            println "Helm Deploy UAT"
                        }
                    }    
                }
              } // stages UAT
            } // UAT
      } // stages
   post {
     cleanup {
       cleanWs()
       deleteDir()
      }
   }
  }
}