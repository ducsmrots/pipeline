import org.jenkinsci.plugins.pipeline.modeldefinition.Utils


def call () {
def jobnameparts = JOB_NAME.tokenize('/') as String[]
def image = jobnameparts[0]
def config = readYaml text: "${env.PIPELINE_CONFIG}"

pipeline {
  agent {
            label 'worker1'
        }
    
        stages {
            stage('Build docker image') {
            when {
                branch 'master'
            }
            steps {
                script {
                    sh "sudo docker build . -t ${image}:${BRANCH_NAME}"
                }
              }
            }
            stage('Integration test') {
              when {
                branch 'master'
              }
              steps {
                script {
                    sh "sudo docker-compose up -d"
                    sh "sudo docker rmi -f ${image}:${BRANCH_NAME}"
                    sh "sudo docker container rm ${image}"
                }
              }
            }
        }
       post {
          cleanup {
              cleanWs()
              deleteDir()
          }
      }
}
}